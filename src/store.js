import Vue from 'vue'
import Vuex from 'vuex'

import main from './store/main'
import users from './store/users'

Vue.use(Vuex);

export const store = new Vuex.Store({
    modules: {
        main,
        users
    }
});