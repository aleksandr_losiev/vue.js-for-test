export default {
    methods: {
        closePopup() {
            this.$store.commit('main/setPopup', '');
        }
    }
}