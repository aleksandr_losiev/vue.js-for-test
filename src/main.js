import Vue from 'vue'
import App from './App.vue'
import router from './router'
import {store} from './store'
import Cookies from 'vue-cookies'

Vue.use(Cookies);
Vue.config.productionTip = false;

new Vue({
    el: '#app',
    router: router,
    store: store,
    render: h => h(App)
});
