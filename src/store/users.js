const state = {
    token: '',
    userData: '',
    usersLiked: [],
    usersAll: []
};

const mutations = {
    setToken(state, token) {
        state.token = token;
    },
    setUserData(state, userData) {
        state.userData = userData;
    },
    setUsersLiked(state, usersLiked) {
        state.usersLiked = usersLiked;
    },
    setUsersAll(state, usersAll) {
        state.usersAll = usersAll;
    },
};

export default {
    namespaced: true,
    state,
    mutations
}