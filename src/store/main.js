const state = {
    popup: ''
};

const mutations = {
    setPopup(state, popup) {
        state.popup = popup;
    },
};

export default {
    namespaced: true,
    state,
    mutations
}