import Vue from 'vue'
import Router from 'vue-router'

import Main from './templates/Main'
import Profile from './templates/Profile'
import User from './templates/User'
import Favorites from './templates/Favorites'

Vue.use(Router);

const router = new Router({
    routes: [
        {
            path: '/',
            name: 'main',
            component: Main
        },
        {
            path: '/user',
            name: 'user',
            component: User
        },
        {
            path: '/profile',
            name: 'profile',
            component: Profile
        },
        {
            path: '/favorites',
            name: 'favorites',
            component: Favorites
        }
    ],
});

export default router;
