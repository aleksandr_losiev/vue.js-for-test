// import vue from 'Vue'
import axios from 'axios'

const API_URL = 'http://test-losiev.webdill.com/api';

export class API {

    headers() {
        return {
            post: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            token: {
                'Authorization': window.$cookies.get('token')
            },
            postToken: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': window.$cookies.get('token')
            },
            file: {
                'Content-Type': 'multipart/form-data',
                'Authorization': window.$cookies.get('token')
            }
        }
    }

    getImagesDir() {
        return 'http://test-losiev.webdill.com/public/images/user/';
    }

    getUser() {
        let url = `${API_URL}/user`;

        return axios({
            method: 'GET',
            url: url,
            headers: this.headers().token,
        });
    }

    getUsers() {
        let url = `${API_URL}/users`;

        return axios.get(url).then(response => response.data);
    }

    updateUser(data) {
        let url = `${API_URL}/user/update`;

        return axios({
            method: 'post',
            url: url,
            data: this.objectToUrl(data),
            headers: this.headers().postToken
        })
    }

    updateUserImage(file) {
        let url = `${API_URL}/user/image`;

        return axios({
            method: 'post',
            url: url,
            data: file,
            headers: this.headers().file
        })
    }

    registerUser(data) {
        let url = `${API_URL}/register`;

        return axios({
            method: 'post',
            url: url,
            data: this.objectToUrl(data),
            headers: this.headers().post
        })
    }

    loginUser(data) {
        let url = `${API_URL}/login`;

        return axios({
            method: 'post',
            url: url,
            data: this.objectToUrl(data),
            headers: this.headers().post
        })
    }

    logout() {
        let url = `${API_URL}/logout`;

        return axios({
            method: 'GET',
            url: url,
            headers: this.headers().token,
        });
    }

    setLike(data) {
        let url = `${API_URL}/like/set`;

        return axios({
            method: 'post',
            url: url,
            data: this.objectToUrl(data),
            headers: this.headers().postToken
        })
    }

    deleteLike(data) {
        let url = `${API_URL}/like/delete`;

        return axios({
            method: 'post',
            url: url,
            data: this.objectToUrl(data),
            headers: this.headers().postToken
        })
    }

    getMyLikedUsers() {
        let url = `${API_URL}/like/get`;

        return axios({
            method: 'GET',
            url: url,
            headers: this.headers().token,
        });
    }

    objectToUrl(obj) {
        return Object.entries(obj).map(([key, val]) => `${key}=${val}`).join('&')
    }
}